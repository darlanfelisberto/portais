<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

        
    public function index($data = array('erros'=>'')){
        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('login/formLogin',$data);
        $this->load->view('template/footer');
    }

    public function logar(){
        $user=$this->input->post('user');
        $passwd=$this->input->post('passwd');
        $this->load->model('LoginModel','loginModel');
        $retorno = $this->loginModel->login($user,$passwd);

        if(isset($retorno)){
            $this->session->idUsuario = $retorno->idUsuario;
            redirect('Welcome');
        }
        else{
            $data['erros']= 'Usuário ou senha invalidos!';
            $this->index($data);
        }
    }
}
