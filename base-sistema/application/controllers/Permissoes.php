<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Permissoes extends CI_Controller
{

    private $breadCrumb = array(
    );
    
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function index()
    {
        $this->load->view('template/start_page');
        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('permissao/buscaPermissao');
        $this->load->view('template/stop_page');
    }
}
