<script type="text/javascript">
            function getDocHeight() {
                var D = document;
                return Math.max(
                    D.body.scrollHeight, D.documentElement.scrollHeight,
                    D.body.offsetHeight, D.documentElement.offsetHeight,
                    D.body.clientHeight, D.documentElement.clientHeight
                );
            }
            
            function resizeMenu(){
                var b= document.getElementById("menu");
                b.style.height = getDocHeight() -50;
            }
            window.onload = function(){
              //  resizeMenu();
            };
            window.onresize= function (){
               // resizeMenu();
            };
            
            document.getElementById("bar").onclick = function(){
                var menu = document.getElementById("menu");
                if(menu.style.display !=="block")
                    menu.style.display = "block";
                else
                    menu.style.display = "none";
            };
        </script>
    </body>
</html>