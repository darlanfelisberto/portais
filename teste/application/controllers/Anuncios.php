<?php

defined('BASEPATH') || exit('No direct script access allowed');

class Anuncios extends CI_Controller {
    
    protected $resposta = array(
        "tipo" =>"",
        "msg" => array()
    );

    public function index() {
        $data = array(
            'header' => 'template/header',
            'footer' => 'template/footer',
            'main_container' =>array('anuncios/meus_anuncios')
        );
        $this->load->view('template/template', $data);
    }

    public function novo() {
        $this->load->model('MD_categorias','m_cat');
        $data = array(
 	        'header' =>'template/header',
 	        'footer' => 'template/footer',
            'main_container' => array('anuncios/formAnuncio'),
            'js' => 'anuncios/js_formAnuncio',
            'categorias'=> $this->m_cat->get_categorias()
        );
        $this->load->view('template/template', $data);
    }

    public function salvar() {
        $msg = array();
        
        $this->load->model('MD_anuncios','m_anuncios');
        $this->load->model('MD_imagens','m_imagens');
        $this->load->library('form_validation',NULL,'validation');
         
        $this->validation->set_rules('titulo', 'Titulo', 'required')
            ->set_rules('descricao', 'Descricao', 'required')
            ->set_rules('preco', 'Preço', 'required')
            ->set_rules('idCategoria', 'Categoria', 'required')
            ->set_rules('nome', 'Nome', 'required')
            ->set_rules('email', 'Email', 'required');
        
        if($this->validation->run() == FALSE){
            $resposta['tipo'] = ERRO_VALIDACAO_FORM;
            $resposta['msg'] = array_values($this->validation->error_array());
            
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(400)
                ->set_output(json_encode($resposta));
        }
        
        
        $msg = array();
        $anuncio = array(
            'titulo' => $this->input->post('titulo',TRUE),
            'descricao' => $this->input->post('descricao',TRUE),
            'preco' => $this->input->post('preco',TRUE),
            'id_categoria' => $this->input->post('idCategoria',TRUE),
//            'telefone' => $this->input->post('telefone',TRUE)
//            '' => $this->input->post('',TRUE),
//            '' => $this->input->post('',TRUE),
        );
        
        $usuraio = array(
            'nome'=> $this->input->post('nome',TRUE),
            'email'=> $this->input->post('email',TRUE),
            'senha' => md5(uniqid(rand(), true))
        );
        
        $this->m_anuncios->inserir_anuncio($anuncio,$usuraio);
        $path_imagens = "C:\imagens\\";
        
        foreach ($_FILES as $file) {
            if($file['size'] <= TAMANHO_IMAGEM_ANUNCIO){
                $imagem = array();
                $imagem['nome'] = $file['name'];
                $imagem['id_anuncio'] = $anuncio['id_anuncio'];
                $this->m_imagens->insert_imagem($imagem);
                move_uploaded_file($file['tmp_name'], $path_imagens.$file['name']);
            }else{
                array_push($msg, 'Tamanho da imagen'.$file['name'].' deve ser menor que '.TAMANHO_IMAGEM_ANUNCIO.'.');
            }
        }
        
        array_push($msg,'Anúncio cadastrado com sucesso!');
        $resposta['tipo'] = SUCESSO_SUBMIT_FORM;
        $resposta['msg']  = $msg;
        $resposta['href'] = 'anuncios/meusanuncios';
        
         return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($resposta));
        
    }
    
    public function  meusanuncios(){
        $this->load->model('MD_anuncios','m_adverts');
        $data = array(
 	        'header' =>'template/header',
 	        'footer' => 'template/footer',
            'main_container' => array('anuncios/meus_anuncios'),
            'js' => 'anuncios/js_formAnuncio',
            'my_adverts'=> $this->m_adverts->get_my_adverts()
        );
        $this->load->view('template/template', $data);
    }

}
