<?php
class MD_anuncios extends CI_Model{
    
    static $table = 'anuncios';
    
    public function inserir_anuncio(&$anuncio,&$usuario){
        $CI =& get_instance();
        $CI->load->model('MD_usuario','m_usuario');
        
        $old = $CI->m_usuario->get_usuario_por_email($usuario['email']);
        if(isset($old)){
            $usuario = $old;
            $anuncio['id_usuario'] = $usuario->id_usuario;
        }else{
            $anuncio['id_usuario'] = $CI->m_usuario->insert_usuario($usuario);
        }
        
        $this->db->insert(MD_anuncios::$table,$anuncio);
        $anuncio['id_anuncio'] = $this->db->insert_id();   
    }
    
    public function get_my_adverts(){
        $result = $this->db->query('select 
            a.id_anuncio,
            a.titulo,
            a.dt_add,
            a.preco,
            a.descricao,
            i.id_imagem,
            i.nome
             from anuncios a
            left join imagens i on i.id_anuncio = a.id_anuncio and i.principal = 1 
            left join cidade c on c.id_cidade = a.id_cidade
            order by a.id_anuncio desc');
        return $result->result();
    }
    
}