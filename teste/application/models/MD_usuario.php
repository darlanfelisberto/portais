<?php
class MD_usuario extends CI_Model{
    static $table ='usuarios';
    
   public function insert_usuario(&$usuario){
       $this->db->insert(MD_usuario::$table,$usuario);
       $usuario['id_usuario'] = $this->db->insert_id();
    }
    
    public function get_usuario_por_email($email){
        $sql = "select * from ".MD_usuario::$table." where email like '%". $this->db->escape_like_str($email)."%'";
        $result = $this->db->query($sql,$email);
        return $result->row();       
    }
}