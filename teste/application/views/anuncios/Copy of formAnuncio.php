<div class="row">
    <div class="col-md-9 page-content">
        <div class="inner-box category-content">
            <h2 class="title-2 uppercase">
                <strong> 
                    <i class="icon-docs"></i> Adicionando um novo anúncio
                </strong>
            </h2>

            <div class="row">
                <div class="col-sm-12">

                    <form class="form-horizontal" enctype="multipart/form-data" id="formNovoAnuncio" method="post" action="anuncios/salvar">

                        

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="textarea">Imagens</label>
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <div class="file-loading">
                                        <input id="file-fr" name="file-fr[]" type="file" multiple>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="textarea">Imagens</label>
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <div class="input-group file-caption-main">
                                        <div class="file-caption form-control kv-fileinput-caption"
                                             tabindex="500">
                                            <span id="mini-file-1"></span>
                                            <span id="nome-fFile-1" class="file-caption-name" style="margin-left: 1em;"></span>
                                        </div>
                                        <div class="input-group-btn input-group-append">
                                            <button type="button" tabindex="500" title="Remover arquivos selecionados" class="btn btn-default btn-secondary fileinput-remove fileinput-remove-button"><i class="glyphicon glyphicon-trash"></i>  <span class="hidden-xs">Remover</span></button>
                                            <button type="button" tabindex="500" title="Interromper envio em andamento" class="btn btn-default btn-secondary kv-hidden fileinput-cancel fileinput-cancel-button"><i class="glyphicon glyphicon-ban-circle"></i>  <span class="hidden-xs">Cancelar</span></button>

                                            <div tabindex="500" class="btn btn-primary btn-file">
                                                <i class="glyphicon glyphicon-folder-open"></i>&nbsp;  
                                                <span class="hidden-xs">Procurar…</span>
                                                <input id="file-1" name="file-1" type="file">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                     


                        <div class="card bg-light card-body mb-3" style="display: none;">
                            <h3>
                                <i class=" icon-certificate icon-color-1"></i> Make your Ad Premium
                            </h3>

                            <p>
                                Premium ads help sellers promote their product or service by getting their ads more visibility with more buyers and sell what they want faster. <a href="help.html">Learn more</a>
                            </p>

                            <div class="form-group row">
                                <table class="table table-hover checkboxtable">
                                    <tbody>
                                        <tr>
                                            <td>

                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio" name="exampleRadios" id="optionsRadios0" value="option1" checked=""/>
                                                        Regular List
                                                    </label>
                                                </div>

                                            </td>
                                            <td><p>$00.00</p></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio" name="exampleRadios" id="optionsRadios1" value="option1" />
                                                        <strong> Urgent Ad</strong>
                                                    </label>
                                                </div>

                                            </td>
                                            <td><p>$10.00</p></td>
                                        </tr>
                                        <tr>
                                            <td>

                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio" name="exampleRadios" id="optionsRadios2" value="option2">
                                                        <strong> Top of the Page Ad</strong>
                                                    </label>
                                                </div>


                                            </td>
                                            <td><p>$20.00</p></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio" name="exampleRadios" id="optionsRadios3" value="option3">
                                                        <strong> Top of the Page Ad + Urgent Ad</strong>
                                                    </label>
                                                </div>


                                            </td>
                                            <td><p>$40.00</p></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group row">
                                                    <div class="col-sm-8">
                                                        <select class="form-control" name="Method" id="PaymentMethod">
                                                            <option value="2">Select Payment Method</option>
                                                            <option value="3">Credit / Debit Card</option>
                                                            <option value="5">Paypal</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><p>
                                                    <strong>Payable Amount : $40.00</strong>
                                                </p></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>



                        <!-- Button  -->
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>

                            <div class="col-sm-8">
                                <button id="formNovoAnuncioBTNEnviar" class="btn btn-success btn-lg">Enviar</button>
                                <a href="#" id="formNovoAnuncioBTNEnviar" class="btn btn-success btn-lg">Enviar</a>
                            </div>
                        </div>


                    </form>

                </div>
            </div>
        </div>
    </div>
    <!-- /.page-content -->

    <div class="col-md-3 reg-sidebar">
        <div class="reg-sidebar-inner text-center">
            <div class="promo-text-box">
                <i class=" icon-picture fa fa-4x icon-color-1"></i>

                <h3>
                    <strong>Post a Free Classified</strong>
                </h3>

                <p>Post your free online classified ads with us. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>

            <div class="card sidebar-card">
                <div class="card-header uppercase">
                    <small><strong>How to sell quickly?</strong></small>
                </div>
                <div class="card-content">
                    <div class="card-body text-left">
                        <ul class="list-check">
                            <li>Use a brief title and description of the item</li>
                            <li>Make sure you post in the correct category</li>
                            <li>Add nice photos to your ad</li>
                            <li>Put a reasonable price</li>
                            <li>Check the item before publish</li>

                        </ul>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <!--/.reg-sidebar-->
</div>
