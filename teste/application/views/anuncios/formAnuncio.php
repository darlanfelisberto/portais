<div class="row">
    <div class="col-md-9 page-content">
        <div class="inner-box category-content">
            <h2 class="title-2 uppercase">
                <strong> 
                    <i class="icon-docs"></i> Adicionando um novo anúncio
                </strong>
            </h2>

            <div class="row">
                <div class="col-sm-12">

                    <form enctype="multipart/form-data" method="post" name="fileinfo" class="form-horizonta" id="formNovoAnuncio">
                        
                        <div class="form-group row">
                            <div class="col-sm-8" id="mensagensV">
                                
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="idCategoria">Categoria</label>
                            <div class="col-sm-8">
                                <select name="idCategoria" id="category-group" class="form-control">
                                    <option></option>
                                    <?php
                                        $options='';
                                        foreach ($categorias as $value) {
                                            $options.='<option value=\''.$value->id_categoria.'\'>'.$value->descricao.'</option>';
                                        }
                                        echo $options;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-3 col-form-label" for="tipoAnunciante">Tipo</label>
                            <div class="col-sm-8">
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="tipoAnunciante" id="inlineRadio1" value="1">
                                        Particular
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="tipoAnuncio" id="inlineRadio2" value="2">
                                        Empresa
                                    </label>
                                </div>


                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="titulo">Titulo</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="titulo" name="titulo">
                                <small id="" class="form-text text-muted"></small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="descricao">Descrição</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" name="descricao" rows="3" cols="3"></textarea>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="preco">Preço</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input id="anuncioPreco" type="text" class="form-control" aria-label="Preço" name="preco" >
                                </div>
                            </div>
                        </div>
                        
                        <!-- Text input-->
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="telefone">Telefone</label>
                            <div class="col-sm-8">
                                <input id="telefone" name="telefone"  class="form-control input-md" required="" type="text"  >
                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="idEstado">Estado</label>
                            <div class="col-sm-8">
                                <select id="estado" class="form-control" name="idEstado">
                                    <option value="1">Option one</option>
                                    <option value="2">Option two</option>
                                </select>
                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="idCidade">Cidade</label>
                            <div class="col-sm-8">
                                <select id="seller-area" name="idCidade" class="form-control" >
                                    <option value="1">Option one</option>
                                    <option value="2">Option two</option>
                                </select>
                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <div class="col-sm-3 col-form-label">
                                <label  for="file-1">Imagens</label>
                            </div>
                            
                            <div class="col-lg-8">
                                <div > 
                                    <i class="fas fa-plus" onclick="novaImagen();"></i>
                                    <div class="form-group" id="colecaoImagens">
                                        <div class="input-group file-caption-main">
                                            <div class="file-caption form-control kv-fileinput-caption"
                                                 tabindex="500">
                                                <span id="mini-file-1"></span>
                                                <span id="nome-file-1" class="file-caption-name" style="margin-left: 1em;"></span>
                                            </div>
                                            <div class="input-group-btn input-group-append">
                                                <button type="button" tabindex="500" title="Remover arquivos selecionados" class="btn btn-default btn-secondary fileinput-remove fileinput-remove-button"><i class="far fa-trash-alt"></i></button>

                                                <div tabindex="500" class="btn btn-primary btn-file"> 
                                                    <span class="hidden-xs">Procurar</span>
                                                    <input id="file-1" name="file-1" type="file">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                           <div class="content-subheading">
                            <i class="icon-user fa"></i> <strong>Informações do Vendedor</strong>
                        </div>

                        <!-- Text input-->
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="nome">Nome</label>
                            <div class="col-sm-8">
                                <input id="textinput-name" name="nome"  class="form-control input-md" type="text">
                            </div>
                        </div>

                        <!-- Appended checkbox -->
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="email">Email</label>
                            <div class="col-sm-8">
                                <input id="email" name="email" class="form-control" required="" type="email">
                            </div>
                        </div>

                        


                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>

                            <div class="col-sm-8">
                                <button id="formNovoAnuncioBTNEnviar" class="btn btn-success btn-lg">Enviar</button>
                                <a href="#" id="formNovoAnuncioBTNEnviar" class="btn btn-success btn-lg">Enviar</a>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<span id="dev"></span>

<!-- /.page-content -->