<script>
    var cont= 1;
    
    $(document).ready(function() {
        $('[name=telefone]').mask('(00)00000-0000');
        $('#anuncioPreco').mask('#.##0,00',{reverse: true});
    });
    
    
    function changeFileInput(evt) {
        var fileInfo =  evt.currentTarget.parentElement.parentElement.parentElement.children[0];
        var files = evt.target.files;

        var output = [];
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();

            reader.onload = (function (theFile) {
                return function (e) {
                    fileInfo.children[0].innerHTML =['<img class="miniatura" src="', e.target.result, '" title="', escape(theFile.name), '"/>'].join('');
                    fileInfo.children[1].innerHTML = escape(theFile.name);
                };
            })(f);

            reader.readAsDataURL(f);
        }
    }

    document.getElementById('file-1').addEventListener('change', changeFileInput, false);

    $("#formNovoAnuncio").submit(function (event) {
        event.preventDefault();
        removeErrosValidacao();
        var myForm = new FormData(document.getElementById('formNovoAnuncio'));

        $.ajax({
            url: "anuncios/salvar",
            data: myForm,
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            async: false,
            success: function (e) {
                sucessoResposta(e);
            },
            error: function (e) {
                erroResposta(e);
            }
        });
        return false;
    });

    function novaImagen(){
        cont++;
        if(cont >5)
            return;
        var div = document.createElement('div');
        div.className = 'input-group file-caption-main';
        div.id = 'idImg-'+cont;
        
        var divIn1 = document.createElement('div');
        divIn1.className = 'file-caption form-control kv-fileinput-caption';
        
        div.appendChild(divIn1);
        
        var miniImg = document.createElement('span');
        miniImg.id='miniImg-'+cont;
        divIn1.appendChild(miniImg);
        
        var nomeImg = document.createElement('span');
        nomeImg.id = 'nomeImg-'+cont;
        nomeImg.className = 'file-caption-name';
        divIn1.appendChild(nomeImg);
        
        var divIn2 = document.createElement('div');
        divIn2.className = 'input-group-btn input-group-append';
        div.appendChild(divIn2);
        
        var btnRemover = document.createElement('button');
        btnRemover.className = 'btn btn-default btn-secondary';
        btnRemover.innerHTML = "<i class='far fa-trash-alt'></i>";
        btnRemover.addEventListener('click', removeImagen, false);
        divIn2.appendChild(btnRemover);
        
        var divProc = document.createElement('div');
        divProc.className = 'btn btn-primary btn-file';
        divProc.innerHTML = "<span class='hidden-xs'>Procurar</span>";
        divIn2.appendChild(divProc);
        
        var inputFile = document.createElement('input');
        inputFile.type = 'file';
        inputFile.name = 'file-'+cont;
        inputFile.addEventListener('change', changeFileInput, false);
        divProc.appendChild(inputFile);
        
        var colecaoImagens = document.getElementById('colecaoImagens');
        colecaoImagens.appendChild(div);
    }

    function removeImagen(evt){
        var e = evt.currentTarget.parentElement.parentElement;
        e.remove();
        cont--;
        
    }
    
    function sucessoResposta(e){
        var msg = $('#mensagensV');
        msg.empty();
        var resposta = e;
        for(var c = 0;c < resposta.msg.length;c++){
            p = document.createElement('p');
            p.className = 'col-sm-11 alert alert-success sem-margin';
            p.textContent = resposta.msg[c];
            msg.append(p);
        }
        window.location.href = resposta.href;
    }
    
    function  criaLinha(){
        var divErro = document.createElement('div');
        divErro.className = 'form-group row';
        divErro.id = 'form-validation';
        var divRow = document.createElement('div');
        divRow.className = 'col-sm-11 alert alert-danger sem-margin';
        divErro.appendChild(divRow);
        return divErro;
    }
    
    function erroResposta(e){
        var msg = $('#mensagensV');
        msg.empty();
        
        if(e.responseJSON === undefined){
            document.getElementById("dev").innerHTML = e.responseText;
        }else{
            var resposta = e.responseJSON;
            switch(resposta.tipo){
                case '<?= ERRO_VALIDACAO_FORM ?>':
                    for(var c = 0;c < resposta.msg.length;c++){
                       p = document.createElement('p');
                        p.className = 'col-sm-11 alert alert-danger sem-margin';
                        p.textContent = resposta.msg[c];
                        msg.append(p);
                    }
                break;
            }     
        }
    }
    
    function removeErrosValidacao(){
        try {
            document.getElementById('form-validation').remove();   
        }catch(e){
            console.log('sem erros de validação');
        }
    }
</script>