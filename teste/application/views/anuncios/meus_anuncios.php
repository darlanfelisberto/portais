<div class="row">
    <div class="col-md-3 page-sidebar">
        <aside>
            <div class="inner-box">
                <div class="user-panel-sidebar">
                    <div class="collapse-box">
                        <h5 class="collapse-title no-border"> My Classified <a class="pull-right" aria-expanded="true" data-toggle="collapse" href="#MyClassified"><i class="fa fa-angle-down"></i></a></h5>

                        <div id="MyClassified" class="panel-collapse collapse show">
                            <ul class="acc-list">
                                <li><a href="account-home.html"><i class="icon-home"></i> Personal Home </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <!-- /.collapse-box  -->
                    <div class="collapse-box">
                        <h5 class="collapse-title"> My Ads <a class="pull-right" aria-expanded="true" data-toggle="collapse" href="#MyAds"><i class="fa fa-angle-down"></i></a>
                        </h5>

                        <div id="MyAds" class="panel-collapse collapse show">
                            <ul class="acc-list">
                                <li class="active"><a href="account-myads.html"><i class="icon-docs"></i> My
                                        ads <span class="badge badge-secondary">42</span> </a></li>
                                <li><a href="account-favourite-ads.html"><i class="icon-heart"></i>
                                        Favourite ads <span class="badge badge-secondary">42</span> </a></li>
                                <li><a href="account-saved-search.html"><i class="icon-star-circled"></i>
                                        Saved search <span class="badge badge-secondary">42</span> </a></li>
                                <li><a href="account-archived-ads.html"><i class="icon-folder-close"></i>
                                        Archived ads <span class="badge badge-secondary">42</span></a></li>
                                <li><a href="account-pending-approval-ads.html"><i class="icon-hourglass"></i> Pending approval <span class="badge">42</span></a></li>

                            </ul>
                        </div>
                    </div>

                    <!-- /.collapse-box  -->
                    <div class="collapse-box">
                        <h5 class="collapse-title"> Terminate Account <a class="pull-right" aria-expanded="true" data-toggle="collapse" href="#TerminateAccount"><i class="fa fa-angle-down"></i></a></h5>

                        <div id="TerminateAccount" class="panel-collapse collapse show">
                            <ul class="acc-list">
                                <li><a href="account-close.html"><i class="icon-cancel-circled "></i> Close
                                        account </a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.collapse-box  -->
                </div>
            </div>
            <!-- /.inner-box  -->

        </aside>
    </div>
    <!--/.page-sidebar-->

    <div class="col-md-9 page-content">
        <div class="inner-box">
            <h2 class="title-2"><i class="icon-docs"></i> My Ads </h2>

            <div class="table-responsive">
                <div class="table-action">
                    <label for="checkAll">
                        <input type="checkbox" id="checkAll">
                        Select: All | <a href="#" class="btn btn-sm btn-danger">Delete <i class="glyphicon glyphicon-remove "></i></a> </label>

                    <div class="table-search pull-right col-sm-7">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-5 control-label text-right">Search <br>
                                    <a title="clear filter" class="clear-filter" href="#clear">[clear]</a>
                                </label>

                                <div class="col-sm-7 searchpan">
                                    <input type="text" class="form-control" id="filter">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table id="addManageTable" class="table table-striped table-bordered add-manage-table table demo footable-loaded footable" data-filter="#filter" data-filter-text-only="true">
                    <thead>
                        <tr>
                            <th data-type="numeric" data-sort-initial="true"></th>
                            <th> Photo</th>
                            <th data-sort-ignore="true"> Adds Details</th>
                            <th data-type="numeric"> Price</th>
                            <th> Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($my_adverts as $row) {  ?>
                        <tr>
                            <td style="width:2%" class="add-img-selector">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">
                                    </label>
                                </div>
                            </td>
                            <td style="width:14%" class="add-img-td">
                                <a href="ads-details.html">
                                    <img class="thumbnail  img-responsive" src="imagens/item/<?= $row->id_imagem ?>" alt="img">
                                </a>
                            </td>
                            <td style="width:58%" class="ads-details-td">
                                <div>
                                    <p><strong> <a href="ads-details.html" title="Brand New Nexus 4"><?= $row->titulo ?></a> </strong></p>
                                    <p><strong> Adicionado </strong>:<?= date("d/m/Y", strtotime($row->dt_add)) ?> </p>
                                    <p><strong>Visitors </strong>: 221 <strong>Located In:</strong> New York </p>
                                </div>
                            </td>
                            <td style="width:16%" class="price-td">
                                <div><strong> <?= $row->preco ?></strong></div>
                            </td>
                            <td style="width:10%" class="action-td">
                                <div>
                                    <p>
                                        <a class="btn btn-primary btn-sm" href="anuncios/editar/<?= $row->id_anuncio ?>"> <i class="fa fa-edit"></i> Editar </a>
                                    </p>
                                    <p>
                                        <a class="btn btn-danger btn-sm" href="anuncios/excluir/<?= $row->id_anuncio ?>"> <i class=" fa fa-trash"></i> Excluir </a>
                                    </p>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>

                    </tbody>
                </table>
            </div>
            <!--/.row-box End-->

        </div>
    </div>
    <!--/.page-content-->
</div>