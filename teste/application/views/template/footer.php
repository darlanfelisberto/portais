<footer class="main-footer">
    	<div class="footer-content">
    		<div class="container">
    			<div class="row">

    				<div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
    					<div class="footer-col">
    						<h4 class="footer-title">About us</h4>
    						<ul class="list-unstyled footer-nav">
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#">About Company</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#">For Business</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#">Our Partners</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#">Press Contact</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#">Careers</a></li>
    						</ul>
    					</div>
    				</div>

    				<div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
    					<div class="footer-col">
    						<h4 class="footer-title">Help &amp; Contact</h4>
    						<ul class="list-unstyled footer-nav">
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#">
    								Stay Safe Online
    							</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#">
    								How to Sell</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#">
    								How to Buy
    							</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#">Posting Rules
    							</a></li>

    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#">
    								Promote Your Ad
    							</a></li>

    						</ul>
    					</div>
    				</div>

    				<div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
    					<div class="footer-col">
    						<h4 class="footer-title">More From Us</h4>
    						<ul class="list-unstyled footer-nav">
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/faq.html">FAQ
    							</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/blogs.html">Blog
    							</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#">
    								Popular Searches
    							</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#"> Site Map
    							</a></li> <li><a href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#"> Customer Reviews
    						</a></li>


    						</ul>
    					</div>
    				</div>
    				<div class=" col-xl-2 col-xl-2 col-md-2 col-6  ">
    					<div class="footer-col">
    						<h4 class="footer-title">Account</h4>
    						<ul class="list-unstyled footer-nav">
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/account-home.html"> Manage Account
    							</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/login.html">Login
    							</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/signup.html">Register
    							</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/account-myads.html"> My ads
    							</a></li>
    							<li><a href="http://templatecycle.com/bootclassified/v4.2/dist/seller-profile.html"> Profile
    							</a></li>
    						</ul>
    					</div>
    				</div>
    				<div class=" col-xl-4 col-xl-4 col-md-4 col-12">
    					<div class="footer-col row">

    						<div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
    							<div class="mobile-app-content">
    								<h4 class="footer-title">Mobile Apps</h4>
    								<div class="row ">
    									<div class="col-6  ">
    										<a class="app-icon" target="_blank" href="https://itunes.apple.com/">
    											<span class="hide-visually">iOS app</span>
    											<img src="./resource/app_store_badge.svg" alt="Available on the App Store">
    										</a>
    									</div>
    									<div class="col-6  ">
    										<a class="app-icon" target="_blank" href="https://play.google.com/store/">
    											<span class="hide-visually">Android App</span>
    											<img src="./resource/google-play-badge.svg" alt="Available on the App Store">
    										</a>
    									</div>
    								</div>
    							</div>
    						</div>

    						<div class="col-sm-12 col-xs-6 col-xxs-12 no-padding-lg">
    							<div class="hero-subscribe">
    								<h4 class="footer-title no-margin">Follow us on</h4>
    								<ul class="list-unstyled list-inline footer-nav social-list-footer social-list-color footer-nav-inline">
    									<li><a class="icon-color fb" title="" data-placement="top" data-toggle="tooltip" href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#" data-original-title="Facebook"><i class="fa fa-facebook"></i> </a></li>
    									<li><a class="icon-color tw" title="" data-placement="top" data-toggle="tooltip" href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#" data-original-title="Twitter"><i class="fa fa-twitter"></i> </a></li>
    									<li><a class="icon-color gp" title="" data-placement="top" data-toggle="tooltip" href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#" data-original-title="Google+"><i class="fa fa-google-plus"></i> </a></li>
    									<li><a class="icon-color lin" title="" data-placement="top" data-toggle="tooltip" href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#" data-original-title="Linkedin"><i class="fa fa-linkedin"></i> </a></li>
    									<li><a class="icon-color pin" title="" data-placement="top" data-toggle="tooltip" href="http://templatecycle.com/bootclassified/v4.2/dist/index.html#" data-original-title="Linkedin"><i class="fa fa-pinterest-p"></i> </a></li>
    								</ul>
    							</div>

    						</div>
    					</div>
    				</div>
    				<div style="clear: both"></div>

    				
    			</div>
    		</div>
    	</div>
    </footer>