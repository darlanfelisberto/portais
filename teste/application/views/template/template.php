<!DOCTYPE html>
<!-- saved from url=(0060)http://templatecycle.com/bootclassified/v4.2/dist/index.html -->
<html lang="pt-br" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="<?php echo base_url()?>>">


<title>BOOTCLASIFIED - Responsive Classified Theme</title>
<!-- Bootstrap core CSS -->
<link href="./resource/bootstrap.css" rel="stylesheet">


<link href="./resource/style.css" rel="stylesheet">

<!-- styles needed for carousel slider -->
<link href="./resource/owl.carousel.css" rel="stylesheet">
<link href="./resource/owl.theme.css" rel="stylesheet">

<!-- bxSlider CSS file -->
<link href="./resource/jquery.bxslider.css" rel="stylesheet">
<link href="./resource/css/fileinput.min.css" rel="stylesheet">
<script src="./resource/js/jquery-3.3.1.min.js"></script>
<script src="./resource/js/jquery.mask.min.js"></script>

<!-- Just for debugging purposes. -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

<!-- include pace script for automatic web page progress bar  -->



</head>
<body class="  pace-done">
	<div class="pace  pace-inactive">
		<div class="pace-progress" data-progress-text="100%" data-progress="99" style="width: 100%;">
			<div class="pace-progress-inner"></div>
		</div>

		<div class="pace-activity"></div>
	</div>
	<div id="wrapper">
    
    	<?php if(isset($header)) $this->view($header);?>
    
        <div class="main-container">
			<div class="container">
    					
                <?php
                    foreach ( $main_container as $item ) {
                            $this->view($item);
                    }

                    // require 'principais_categorias.php'
                    // require 'ultimosAnuncios.php'
                    // require 'maisBuscadas.php'
                ?>            
    
            </div>
		</div>
		<!-- /.main-container -->
    
        <?php //require 'totais.php';?>
    
       	<?php //require 'sugestoes.php';?>
    	
        <?php if(isset($footer))$this->view($footer);?>
    
    </div>
	<!-- /.wrapper -->


	
    <?php foreach ($js as $script) {
        <script src="{$js}"></script>
    }
    $this->view($js);?>

	<div class="autocomplete-suggestions" style="position: absolute; display: none; max-height: 300px; z-index: 9999;"></div>
</body>
</html>
